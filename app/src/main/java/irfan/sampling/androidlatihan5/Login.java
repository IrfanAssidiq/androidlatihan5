package irfan.sampling.androidlatihan5;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import irfan.sampling.androidlatihan5.helpers.PrefsHelper;

public class Login extends AppCompatActivity {

    EditText et_nama, et_pass;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        et_nama = findViewById(R.id.nama);
        et_pass = findViewById(R.id.pass);
        Button btn = findViewById(R.id.btn);

        boolean ceklogin = PrefsHelper.sharedInstance(getApplicationContext()).isUserLogin();
        if(ceklogin){
            startActivity(new Intent(Login.this, MainActivity.class));
            finish();
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_nama.getText().toString().equals("") || et_pass.getText().toString().equals("")){
                    Toast.makeText(Login.this, "password atau user kosong", Toast.LENGTH_SHORT).show();
                }else{
                    if(et_nama.getText().toString().equals("admin") &&
                            et_pass.getText().toString().equals("admin123")){
                        startActivity(new Intent(Login.this, MainActivity.class));
                        PrefsHelper.sharedInstance(getApplicationContext()).cekLogin(true);
                        PrefsHelper.sharedInstance(getApplicationContext()).setNamaDefault("admin");
                        finish();
                    }else if(et_nama.getText().toString().equals("pengguna") &&
                            et_pass.getText().toString().equals("pengguna123")){
                        startActivity(new Intent(Login.this, MainActivity.class));
                        PrefsHelper.sharedInstance(getApplicationContext()).cekLogin(true);
                        PrefsHelper.sharedInstance(getApplicationContext()).setNamaDefault("pengguna");
                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setTitle("Warning")
                .setCancelable(false)
                .setMessage("Apakah Anda Yakin Ingin Keluar?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       finish(); }})
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //
                    }}).show();
    }
}
