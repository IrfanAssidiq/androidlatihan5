package irfan.sampling.androidlatihan5;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import irfan.sampling.androidlatihan5.helpers.BookHelper;
import irfan.sampling.androidlatihan5.helpers.PrefsHelper;

public class MainActivity extends AppCompatActivity {

    Cursor c;
    SimpleCursorAdapter sca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lv = findViewById(R.id.lv_list);
        FloatingActionButton fab = findViewById(R.id.fab);

        BookHelper helper = new BookHelper(this);
        SQLiteDatabase database = helper.getWritableDatabase();

        String[] datax = {"_id", "title", "author"};
        c = database.query("samplebooks", datax,
                null,
                null,
                null,
                null,
                null);
        sca = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2,
                c, new String[]{"title", "author"},
                new int[]{android.R.id.text1, android.R.id.text2},
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        lv.setAdapter(sca);

//        ArrayList<String> data = new ArrayList<>();
//        c.moveToFirst();
//        while (!c.isAfterLast()){
//            String title = c.getString(c.getColumnIndex("title"));
//            data.add(title);
//            c.moveToNext();
//        }
//
//        if(data.isEmpty()){
//            data.add("data belum tersedia");
//        }
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_list_item_1, data);
//        lv.setAdapter(adapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Inserting.class));
            }
        });

        String nama_ = PrefsHelper.sharedInstance(getApplicationContext()).getNamaDefault();

        if(nama_.equals("admin")){
            //registrasikan menu
            registerForContextMenu(lv);
        }else{
            //do nothing
        }


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.edit_menu, menu);
    }

    //membuat method untuk menghapus data buku
    public void deletebooks(long id){
        BookHelper helper = new BookHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        db.delete("samplebooks", "_id=?",
                new String[]{String.valueOf(id)});
        Toast.makeText(this, "data berhasil dihapus", Toast.LENGTH_SHORT).show();

        Cursor x = db.query("samplebooks", new String[]{"_id","title","author"},
                null, null, null, null, null);
        sca.changeCursor(x);
        sca.notifyDataSetChanged();
    }

    //membuat method untuk update data buku
    public void updatebooks(long id){
        BookHelper helper = new BookHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        Cursor e = db.query("samplebooks", new String[]{"title", "author"},
                "_id=?", new String[]{String.valueOf(id)},
                null, null, null);
        e.moveToFirst();
        Intent ii = new Intent(MainActivity.this, Inserting.class);
        ii.putExtra("_id", id);
        ii.putExtra("title", e.getString(e.getColumnIndex("title")));
        ii.putExtra("author", e.getString(e.getColumnIndex("author")));
        startActivity(ii);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.delete :{
                deletebooks(info.id);
            }break;
            case R.id.update:{
                updatebooks(info.id);
            }break;
            default:
                //
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logoutin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case  R.id.toLogout:{
                startActivity(new Intent(MainActivity.this, NamaClass.class));
                finish();
            }break;
            default:
                //
        }
        return super.onOptionsItemSelected(item);
    }
}
