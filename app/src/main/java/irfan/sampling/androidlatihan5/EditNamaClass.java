package irfan.sampling.androidlatihan5;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import irfan.sampling.androidlatihan5.helpers.PrefsHelper;

public class EditNamaClass extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_nama);
        final EditText et_nama = findViewById(R.id.et_nama);
        Button btn_ = findViewById(R.id.btn_);

        btn_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_nama.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Nama Kosong",
                            Toast.LENGTH_SHORT).show();
                }else{
                    PrefsHelper.sharedInstance(getApplicationContext())
                            .setNamaDefault(et_nama.getText().toString());
                    startActivity(new Intent(EditNamaClass.this, NamaClass.class));
                    finish();
                }
            }
        });
    }
}
